import './App.css';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { UserProvider } from './userContext';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
import UnauthorizedAccess from './pages/UnauthorizedAccess';
import AdminDashboard from './pages/AdminDashboard';
import UpdateProduct from './pages/UpdateProduct';
import ProductView from './pages/ProductView';
import Landing from './pages/Landing';
import Highlights from './components/Highlights';
import Profile from './pages/Profile';

function App() {

  const [user, setUser] = useState({
      id: null,
      isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user)
    console.log(localStorage)
  })

  return (
    
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        
        <AppNavBar /> 
        <Container>
          <Routes>
            
            <Route path="/" element={<Landing />} />
            <Route path="/home" element={<Home />} />
            <Route path="/products" element={<Products />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />            
            <Route path="/unauthorizedAccess" element={<UnauthorizedAccess/>} />
            <Route path="/adminDashboard" element={<AdminDashboard />} />
            <Route path="/products/update/:productId" element={<UpdateProduct />} />
            <Route path="/productView/:productId" element={<ProductView />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="*" element={<NotFound />} />
            


          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
