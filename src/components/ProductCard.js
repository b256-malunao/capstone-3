import {Card, Button, Image, Container, Row, Col } from 'react-bootstrap';
import { Link, useNavigate} from 'react-router-dom';
import UserContext from '../userContext';
import { useContext } from 'react';

export default function ProductCard({productProp}) {

	const { name, description, price, _id, image } = productProp;

	const navigate = useNavigate();
	const { user, setUser } = useContext(UserContext);

	return (
		
		<Col className= "justify-content-center">
		 	<Card border="dark" style={{ width: '18rem', height: "30rem"}}>
		      	<Card.Body>
		      		<Card.Img src={image} style={{ width: '16rem' , height: "18rem"}}/>
					<br/><br/>
			      	<Card.Title>{name}</Card.Title>
					
				    <Card.Subtitle><strong>Price:</strong></Card.Subtitle>
				    <Card.Text>{price}</Card.Text>
				    <Button variant="primary" as={Link} to={`/productView/${_id}`}>View Product</Button>    
		      	</Card.Body>
	    	</Card>
	    </Col>
	)
}

