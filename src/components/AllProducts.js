import {Card, Button, ButtonGroup, Table, Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useState , useEffect} from 'react';


export default function AllProducts({allProducts}) {

	const { name, description, price, _id, isActive } = allProducts;

	return (
		<>
		 <Card>
	      <Card.Body>
	      	<Row>
	      	<Col sm={8}>
	        <Card.Title>{name}</Card.Title>
		          <Card.Subtitle>Description:</Card.Subtitle>
		          <Card.Text>{description}</Card.Text>
		          <Card.Subtitle><strong>Price:</strong></Card.Subtitle>
		          <Card.Text>{price}</Card.Text>
		    </Col>
		    <Col sm={4}>
		      	<Button variant="info" as={Link} to={`/products/update/${_id}`}>Update</Button>{' '}
		      	<ButtonGroup>
			        <Button>Archive</Button>
			        <Button>Unarchive</Button>
			      </ButtonGroup>
			</Col>

			</Row>
	      </Card.Body>
	    </Card>


	    </>
	
	)
}