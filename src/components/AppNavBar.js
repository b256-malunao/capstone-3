import { useContext } from 'react';
import { Container, Navbar, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../userContext';

export default function AppNavBar() {

  const { user } = useContext(UserContext);

	return (

		<> 
      <Navbar expand="lg" className="nav-bg">
        <Container className="nav-bg">
          <Navbar.Brand className="nav-bg" as={Link} to="/">Hobby Isekai</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse className="nav-bg" id="basic-navbar-nav">
            <Nav className="ml-auto nav-bg">
              <Nav.Link className="nav-bg" as={NavLink} to="/home">Home</Nav.Link> 

  
              {
                (user.isAdmin) ?
                <>
                <Nav.Link className="nav-bg" as={NavLink} to="/adminDashboard">Admin Dashboard</Nav.Link>
                <Nav.Link className="nav-bg" as={NavLink} to="/products">Products</Nav.Link>
                </> 
                :
                <Nav.Link className="nav-bg" as={NavLink} to="/products">Products</Nav.Link> 
              }
               {
                
                (user.id !== null) ?
                <>
                  <Nav.Link className="nav-bg" as={NavLink} to="/logout">Logout</Nav.Link>
                  <Nav.Link className="nav-bg" as={NavLink} to="/profile">Profile</Nav.Link>
                </>
                :
                <>
                  <Nav.Link className="nav-bg" as={NavLink} to="/login">Login</Nav.Link>
                  <Nav.Link className="nav-bg" as={NavLink} to="/register">Register</Nav.Link>
                </>
              }
              
              
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>

	
	)
}