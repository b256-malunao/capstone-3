import { useState, useEffect, useContext } from 'react';
import { Container, Card, Row, Col, Image, CardGroup, Button} from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import UserContext from '../userContext';
import { BsFillCartDashFill,  BsFillCartPlusFill } from "react-icons/bs";
import Swal from 'sweetalert2';

		export default function ProductView() {

		
// View Products 
			const { user } = useContext(UserContext);
			const { productId } = useParams(); 
			const [image, setImage] = useState("")
			const [name, setName] = useState("");
			const [description, setDescription] = useState("");
			const [price, setPrice] = useState(0);
			const [userId, setUserId] = useState('');
			const [totalAmount, setTotalAmount] =('')
			const [isActive, setIsActive] = useState('false');

			useEffect(() => {

				console.log(productId);

				fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
				// fetch(`http://localhost:4000/products/${productId}`)
				.then(res => res.json())
				.then(data => {

					console.log(data);
					setImage(data.image); 
					setName(data.name);
					setDescription(data.description);
					setPrice(data.price);
				}) 

			})

//End View Products 

// Add quantity

	const [quantity, setQuantity] = useState(0)

	function addToCart() {

		setQuantity(quantity + 1);

	}

	function minusToCart() {

		if(quantity > 0) {
			setQuantity(quantity - 1);

		} else {

			alert("You have no product in your cart")
		}
		
	}

	useEffect(() => {

       
		        if(quantity === 0){
		            setIsActive(true);
		        } else {
		            setIsActive(false);
		        }

		    }, [quantity]);


	const createOrder = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
		// fetch(`http://localhost:4000/users/checkout`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}` 

			},
			body: JSON.stringify({
                    productId: productId,
                    productName: name,
                    quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			
			console.log(data)
			if(data) {
				Swal.fire({
                    title: "ORDER SUCCESS!",
                    icon: "success",
                    text: "The new order was added to the order list."
                })

                setQuantity('')

			} else {
				Swal.fire({
                    title: "ORDER UNSSUCCESSFUL!",
                    icon: "error",
                    text: "Please try again later.",
                })
			}
		})
	}


// End of Add Quantity

			return(
				<Container className="my-3">
					<Row>
						<Col lg={{ span: 3, offset: 1 }}>
							<Card.Img src={image} style={{ width: '18rem' }}/>
						</Col>
						<Col lg={{ span: 6, offset: 1 }}>
							<Card>
								<Card.Body>
								<Row>
									
									<Card.Title>{name}</Card.Title>
									<Card.Subtitle>Description:</Card.Subtitle>
									<Card.Text>{description}</Card.Text>
									<Card.Subtitle>Price:</Card.Subtitle>
									<Card.Text>PhP {price}</Card.Text>
									<Card.Subtitle>Quantity:</Card.Subtitle>
									
									<BsFillCartPlusFill as='btn' onClick={()=> addToCart()}/> <br/>
									<Card.Text className="text-center">{quantity} </Card.Text>
									<BsFillCartDashFill as='btn' onClick={()=> minusToCart()}/> <br/>
									{
										
										(user.isAdmin || user.id === null || isActive) ?
										<>
										<Button variant="primary" disabled >Check Out</Button>
										{' '}
										<Button variant="info" as={Link} to={`/products`}>Back to Products Page</Button>
										</>
										
										:
										<>
										<Button variant="primary" onClick={()=> createOrder(productId)} >Check Out</Button>
										<Button variant="info" as={Link} to={`/products`}>Back to Products Page</Button>
										</>
									}
									
								</Row>	
									
								</Card.Body>		
							</Card>
						</Col>
					</Row>
				</Container>

				
			)
		}
