import { Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function UnauthorizedAccess() {

	return (

		<Card className="my-3">
	      <Card.Header >Unauthorized Access detected!</Card.Header>
	      <Card.Body>
	        <Card.Title>You cannot access this page.</Card.Title>
	        <Card.Text>
	          You are not an admin.
	        </Card.Text>
	        <Button variant="info" as={Link} to={`/home`}>Go Back To HOMEPAGE</Button>
	      </Card.Body>
	    </Card>
	)
}