import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function Register() {

	const { user } = useContext(UserContext);

	const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);

	const navigate = useNavigate();

	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(mobileNo);
	console.log(password1);
	console.log(password2);

	useEffect(() => {

       
        if((firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [firstName, lastName, email, mobileNo, password1, password2]);


    function registerUser(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
		// fetch(`http://localhost:4000/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if(data === true){

                Swal.fire({
                    title: 'Duplicate email found',
                    icon: 'error',
                    text: 'Please provide a different email.'   
                });

            } else {

            	fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                // fetch(`http://localhost:4000/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {

                    console.log(data);

                    if(data === true){

                        // Clear input fields
                        setFirstName('');
                        setLastName('');
                        setEmail('');
                        setMobileNo('');
                        setPassword1('');
                        setPassword2('');

                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            text: 'Welcome! You can now now login and start shopping'
                        });

                      
                        navigate("/login");

                    } else {

                        Swal.fire({
                            title: 'Something wrong',
                            icon: 'error',
                            text: 'Please try again.'   
                        });

                    };

                })
            };

        })

    }

	return (
		
		(user.id !== null) ?
			<Navigate to="/products" />
		:
		<Container fluid className="regForm">
		<h3>Register Form</h3>
		<Form onSubmit={e => registerUser(e)}>

		<Form.Group className="mb-3" controlId="formBasicFirstName">
		        <Form.Label>First Name</Form.Label>
		        <Form.Control type="text" placeholder="Enter first name" value={firstName} onChange={e => setFirstName(e.target.value)}/>
		        <Form.Text className="text-muted">
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="formBasicLastName">
		        <Form.Label>Last Name</Form.Label>
		        <Form.Control type="text" placeholder="Enter last name" value={lastName} onChange={e => setLastName(e.target.value)}/>
		        <Form.Text className="text-muted">
		        </Form.Text>
		      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicMobileNo">
		        <Form.Label>Mobile Number</Form.Label>
		        <Form.Control type="text" placeholder="Enter mobile number" value={mobileNo} onChange={e => setMobileNo(e.target.value)}/>
		      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword">
	        <Form.Label>Password</Form.Label>
	        <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword2">
	        <Form.Label>Verify Password</Form.Label>
	        <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)}/>
	      </Form.Group>

	  	  
	      {
	      	isActive ?
	      		<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
	      	:
	      		<Button variant="outline-primary" type="submit" id="submitBtn" disabled>Submit</Button>
	      }
	      <br/><br/>
	      <h10>Already have an account? <a href="/login">Click here</a> to log in.</h10>
	    </Form>
	   
	    </Container>
	)
}