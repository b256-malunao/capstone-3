
import { useState, useEffect, useContext } from 'react';
import UserContext from '../userContext';
import { Container, Card} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';

export default function Profile() {
	const { user } = useContext(UserContext);

	const[email, setEmail] = useState('');
	const[firstName, setFirstName] = useState('');
	const[lastName, setLastName] = useState('');
	const[mobileNo,setMobileNo] = useState('');
	// const[orders, setOrders] = useState([]);
	const[userId, setUserId] = useState('');
	// const[totalAmount, setTotalAmount] = useState(0);
	// const[product, setProduct] = useState([]);

	// console.log(orders)

	fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
	// fetch('http://localhost:4000/users/details', {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
		})
		.then(res => res.json())
		.then(data => {

		// console.log(data);
		setUserId(data._id)
		setEmail(data.email)
		setLastName(data.lastName);
		setFirstName(data.firstName);
		setMobileNo(data.mobileNo);

		})
	



		// fetch(`http://localhost:4000/users/myOrders/${userId}`, {
	 	// 		headers: {
		// 			Authorization: `Bearer ${localStorage.getItem('token')}`
		// 		}	

		//  })
		//  .then(res => res.json())
		//  .then(data => {

		//  	setOrders(data.map(order => {
		//  		console.log(order)
		//  		setOrders(order)


		//  	}))
		//  })

	
	 




return(

	(user.id === null) ?
	<Navigate to="/login" />

	:
<>
		<Card key={userId} style={{ width: '30rem' }} className='m-3 text-align-center'>
	      <Card.Body>
	        <Card.Title>Profile</Card.Title>
	        <Card.Text><b>Full Name:</b> {firstName} {lastName}</Card.Text>
	        <Card.Text><b>Email:</b> {email}</Card.Text>
	        <Card.Text><b>Mobile Number:</b> {mobileNo}</Card.Text>
	        <Card.Text><b>Orders</b></Card.Text>
	      </Card.Body>
	    </Card>
	{/*<h5> My Orders </h5>*/}

</>
	
)

}