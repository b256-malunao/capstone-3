import Carousel from 'react-bootstrap/Carousel';

export default function Landing() {

	return(
		<>
		<h1 className="text-center"> Hello Anime Lovers! </h1>
		<Carousel >
		      <Carousel.Item interval={1000} className="landing">
		        <img
		          className="d-block w-75"
		          src="https://www.themoviedb.org/t/p/original/xzjZDyqUobuJtkBljhgLH4Fdnye.jpg"
		          alt="First slide"
		        />
		        <Carousel.Caption>
		          <h3 className="landingCaption">That Time I Got Reincarnated as a Slime</h3>
		          <p className="landingCaption">Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
		        </Carousel.Caption>
		      </Carousel.Item>
		      <Carousel.Item interval={1000} className="landing">
		        <img
		          className="d-block w-75"
		          src="https://bookandfilmglobe.com/wp-content/uploads/2021/05/Demon-Slayer-The-Movie.jpg"
		          alt="Second slide"
		        />
		        <Carousel.Caption >
		          <h3 className="landingCaption">Demon Slayer</h3>
		          <p className="landingCaption">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		        </Carousel.Caption>
		      </Carousel.Item>
		      <Carousel.Item interval={1000} className="landing">
		        <img
		          className="d-block w-75 "
		          src="https://i1.wp.com/www.mobileyouth.org/wp-content/uploads/in-another-world-with-my-smartphone-season-2.jpg?fit=1000%2C600&ssl=1"
		          alt="Third slide"
		        />
		        <Carousel.Caption >
		          <h3 className="landingCaption">In anotherworld with my smartphone</h3>
		          <p className="landingCaption">
		            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
		          </p>
		        </Carousel.Caption>
		      </Carousel.Item >
		    </Carousel>
		   </>
	)
}