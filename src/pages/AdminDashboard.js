import { useState, useEffect, useContext } from 'react';
import AllProducts from '../components/AllProducts';
import UserContext from '../userContext';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import { Button, Row, Col, Modal, Form, Card, Table, ButtonGroup, Container, Image } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminDashboard() {

    

    const navigate = useNavigate();

    const { user } = useContext(UserContext);
    const [name, setName] = useState('');
    const [productId, setProductId] = useState("");
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [image, setImage] = useState('');
    const [isActive, setIsActive] = useState(false);
    const [allProducts, setAllProducts] = useState([]);
    console.log(user)

    useEffect(() => {

       
        if(name !== '' && description !== '' && price !== ''){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description, price, image]);

// Show AllProducts

    const fetchAllProducts = () => {

        fetch(`${process.env.REACT_APP_API_URL}/products/all`)
        // fetch('http://localhost:4000/products/all')
        .then(res => res.json())
        .then(data => {

            console.log(data)
            setAllProducts(data)
        })
    }

    useEffect(() => {
        fetchAllProducts()
    }, [])

// End Show AllProducts

// CreateProduct Modal
 const [addShow, setAddShow] = useState(false);

  const addHandleClose = () => setAddShow(false);
  const addHandleShow = () => setAddShow(true);

// End of CREATE PRODUCT Modal

// Create Product

function createProduct (e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
        // fetch('http://localhost:4000/products/create', {

            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                image: image,
            })
        })
        .then(res => res.json())
        .then(data => {
            
            if(data) {

                Swal.fire({
                    title: "You have successfully added a new product!",
                    icon: "success",
                    text: "Product list has been updated"
                
                })

                setName('')
                setDescription('')  
                setPrice('')
                setImage('')

            } else {

                Swal.fire({
                    title: "Unexpected Error found!",
                    icon: "success",
                    text: "Please try again!"
                
                })
            }
        })
    }

    const archiveProduct = (prodId) => {

        console.log(prodId)

        fetch(`${process.env.REACT_APP_API_URL}/products/archive/${prodId}`, {
        // fetch(`http://localhost:4000/products/archive/${prodId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: false
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data)

            if(data) {

                Swal.fire({
                    title: "Product is successfully archived!",
                    icon: 'success',
                    text: "The product is now is the archive folder"
                });

                fetchAllProducts();
            } else {

                Swal.fire({
                    title: "Something went wrong",
                    icon: 'error',
                    text: "Please try again later!"

                })
            }
        })
    }

// End of ArchiveProduct

// UnarchiveProduct

const unarchiveProduct = (prodId) => {

        console.log(prodId)

        fetch(`${process.env.REACT_APP_API_URL}/products/unarchive/${prodId}`, {
        // fetch(`http://localhost:4000/products/unarchive/${prodId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: true
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data)

            if(data) {

                Swal.fire({
                    title: "Product is successfully unarchived!",
                    icon: 'success',
                    text: "The product is no longer in the archive folder"
                });

                fetchAllProducts();
            } else {

                Swal.fire({
                    title: "Something went wrong",
                    icon: 'error',
                    text: "Please try again later!"

                })
            }
        })
    }

// End of UnArchive


// ---- END OF CREATE PRODUCT ----


    return(
            (user.isAdmin !== true) ?
                navigate('/UnauthorizedAccess')
            :
            <>
                <div>
                <Row className="text-center m-3">
                    <Col>
                    <h1>Admin Dashboard</h1>
                    <Button variant="primary" onClick={addHandleShow}>Add New Product</Button>{' '}
                     <Button variant="info">Show User Orders</Button>{' '}
                    </Col>
                </Row>
                </div>
            <div>
                <Row className="text-center m-2">
                    <Col>
                       <Table striped bordered hover >
                          <thead>
                            <tr>
                              <th>Product Name</th>
                              <th>Image</th>
                              <th>Description</th>
                              <th>Price</th>
                              <th>Available</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            
                             {
                                allProducts.map(product => (
                                <tr key={product._id}>
                                    <td>{product.name}</td>
                                    <td><img src={product.image} style={{ width: '6rem' }}/></td>
                                    <td>{product.description}</td>
                                    <td>{product.price}</td>
                                    <td>{product.isActive ? "Yes" : "No"}</td>
                                    <td>
                                        <Button variant="info" as={Link} to={`/products/update/${product._id}`}>View Details/Update</Button>{' '}
                                        <ButtonGroup>
                                            {
                                                (product.isActive) ?
                                                <>
                                                <Button variant="primary" onClick={()=> archiveProduct(product._id)}> Archive </Button>
                                                <Button variant="outline-primary" disabled>Unarchive</Button>
                                                </>
                                                :
                                                <>
                                                <Button variant="outline-primary" disabled>Archive</Button>
                                                <Button variant="primary" onClick={()=> unarchiveProduct(product._id)}>Unarchive</Button>
                                                </>
                                            }
                                            
                                        </ButtonGroup>
                                    </td>
                                </tr>
                                ))
                             }
                            </tbody>
                        </Table>
                    </Col>
                 </Row>
                </div>

        {/*ADD PRODUCT MODAL*/}
            <Modal show={addShow} onHide={addHandleClose}>
                <Modal.Header closeButton>
                  <Modal.Title>Add New Product</Modal.Title>
                </Modal.Header>
                <Modal.Body>
        <Container fluid>
        <Form onSubmit={e => createProduct(e)}>
        
        <Form.Group className="mb-3" controlId="nameProduct">
                <Form.Label>Product Name</Form.Label>
                <Form.Control type="text" placeholder="Enter product name" value={name} onChange={e => setName(e.target.value)}/>
                <Form.Text className="text-muted">
                </Form.Text>
              </Form.Group>

              <Form.Group className="mb-3" controlId="descriptionProduct">
                <Form.Label>Description</Form.Label>
                <Form.Control style={{ height: '12rem' }}as="textarea" placeholder="Enter product description" value={description} onChange={e => setDescription(e.target.value)}/>
                <Form.Text className="text-muted">
                </Form.Text>
              </Form.Group>

          <Form.Group className="mb-3" controlId="priceProduct">
                <Form.Label>Price</Form.Label>
                <Form.Control type="text" placeholder="Enter product price" value={price} onChange={e => setPrice(e.target.value)}/>
              </Form.Group>
            <Form.Group className="mb-3" controlId="imageProduct">
                <Form.Label>Image Url</Form.Label>
                <Form.Control type="text" placeholder="Enter image url" value={image} onChange={e => setImage(e.target.value)}/>
              </Form.Group>
          
          {
            isActive ?
                <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
            :
                <Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
          }
          
        </Form>
        </Container>
                
                </Modal.Body>
                <Modal.Footer>
                  <Button variant="secondary" onClick={addHandleClose} as={Link} to={'/adminDashboard'}>
                    Close
                  </Button>
                </Modal.Footer>
            </Modal>
      {/*END of ADD PRODUCT MODAL*/}
                    
            </>



    )

}
