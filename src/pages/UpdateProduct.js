import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

		export default function UpdateProduct() {

		
			
			const { user } = useContext(UserContext); 
			const { productId } = useParams(); 

			// const navigate = useNavigate();

			const [name, setName] = useState('');
			const [image, setImage] = useState('');
			const [description, setDescription] = useState('');
			const [price, setPrice] = useState(0);
			const [isAvailable,setIsAvailable] = useState(true);
			const [createdOn, setCreatedOn] = useState('');
			const [isActive, setIsActive] = useState(false);
			const [updatedName, setUpdatedName] = useState('');
			const [updatedDescription, setUpdatedDescription] = useState('');
			const [updatedImage, setUpdatedImage] = useState('');
			const [updatedPrice, setUpdatedPrice] = useState(0);

			useEffect(() => {

       
		        if(updatedName !== '' || updatedDescription !== ''|| updatedPrice !== 0 ){
		            setIsActive(true);
		        } else {
		            setIsActive(false);
		        }

		    }, [updatedName, updatedDescription, updatedPrice, updatedImage]);

				fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
				// fetch(`http://localhost:4000/products/${productId}`)
				.then(res => res.json())
				.then(data => {

					console.log(data);
					setName(data.name);
					setDescription(data.description);
					setPrice(data.price);
					setIsAvailable(data.isActive);
					setImage(data.image);
					setCreatedOn(data.createdOn);
				}) 

// Update Product

function productUpdate(e) {

	e.preventDefault()

	fetch(`${process.env.REACT_APP_API_URL}/products/update/${productId}`, {
    // fetch(`http://localhost:4000/products/update/${productId}`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
            name: updatedName,
            description: updatedDescription,
            price: updatedPrice,
            image: updatedImage
        })

    })
    .then(res => res.json())
    .then(data => {

            if(data) {

                Swal.fire({
                    title: "You have successfully updated this product!",
                    icon: "success",
                    text: "Please check product list"
                
                })

                setUpdatedName('')
                setUpdatedDescription('')  
                setUpdatedPrice('')
                setUpdatedImage('')

            } else {

                Swal.fire({
                    title: "Unexpected Error found!",
                    icon: "success",
                    text: "Please try again!"
                
                })
            }

    })

}


			return(
				
				<Container className="mt-5 text-align-center">
					<Row className>
						<Col md={6}>
							<Card>
								<Card.Body className="text-align-center">
									<Card.Title>{name}</Card.Title> <br/>
									 <Card.Img variant="top" src={image} style={{ width: '10rem', height: '12rem'  }}/> <br/><br/>
									<Card.Subtitle>Description:</Card.Subtitle>
									<Card.Text>{description}</Card.Text>
									<Card.Subtitle>Price:</Card.Subtitle>
									<Card.Text>PhP {price}</Card.Text>
									<Card.Subtitle>Available:</Card.Subtitle>
									<Card.Text>{isAvailable ? "Yes" : "No"}</Card.Text>
									<Card.Subtitle>Created On:</Card.Subtitle>
									<Card.Text>{createdOn}</Card.Text>
								</Card.Body>		
							</Card>
						</Col>
						<Col md={6}>
							<>
								<h1> Update Form </h1>
								<Container fluid>
								<Form onSubmit={e => productUpdate(e)}>
								<Form.Group className="mb-3" controlId="nameProduct">
					                <Form.Label>Product Name</Form.Label>
					                <Form.Control type="text" placeholder="Enter new product name" value={updatedName} onChange={e => setUpdatedName(e.target.value)}/>
					                <Form.Text className="text-muted">
					                </Form.Text>
					              </Form.Group>

					              <Form.Group className="mb-3" controlId="descriptionProduct">
					                <Form.Label>Description</Form.Label>
					                <Form.Control style={{ height: '12rem' }} as="textarea" placeholder="Enter new product description" value={updatedDescription} onChange={e => setUpdatedDescription(e.target.value)}/>
					                <Form.Text className="text-muted">
					                </Form.Text>
					              </Form.Group>

					          <Form.Group className="mb-3" controlId="ImageProduct">
					                <Form.Label>Image URL</Form.Label>
					                <Form.Control type="text" placeholder="Enter new image url" value={updatedImage} onChange={e => setUpdatedImage(e.target.value)}/>
					              </Form.Group>
					          <Form.Group className="mb-3" controlId="priceProduct">
					                <Form.Label>Price</Form.Label>
					                <Form.Control type="text" placeholder="Enter new product price" value={updatedPrice} onChange={e => setUpdatedPrice(e.target.value)}/>
					              </Form.Group>
					               {
							            isActive ?
							            <>
							                <Button variant="primary" type="submit" id="submitBtn">Submit</Button>{' '}
							                <Button variant="primary" type="submit" id="submitBtn" as={Link} to={'/adminDashboard'}>Back to Admin Dashboard</Button>
							              </>
							            :
							            <>
							                <Button variant="danger" type="submit" id="submitBtn" disabled>Update</Button>{' '}
							                <Button variant="primary" type="submit" id="submitBtn" as={Link} to={'/adminDashboard'}>Back to Admin Dashboard</Button>
							             </>
							          }
					        </Form>
					        </Container>
							</>
						</Col>
					</Row>
				</Container>
	
			)
		}
