import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {
    const data = {
        title: "Hobby Isekai",
        content: "You can shop with confidence at Hobby Isekai if you're looking for something new to add to your collection of anime figurines because we sell a large selection of amazing Japanese collectibles to really take it to the next level.",
        destination: "/products",
        label: "Shop Now!"
    }

    return (
        <>
            <Banner data={data}/>
            <Highlights />
        </>
    )
}
