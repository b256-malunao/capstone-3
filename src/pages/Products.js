import { useState, useEffect } from 'react';
import ProductCard from '../components/ProductCard';
import {Row} from 'react-bootstrap'

export default function Products() {

	const [ products, setProducts ] = useState([]);

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		// fetch('http://localhost:4000/products/active')
		.then(res => res.json())
		.then(data => {

	

			setProducts(data.map(product => {
				return (
					<ProductCard key={product.id} productProp={product} />
				)
		}))
	})
})

	return(
		<>
			<h1 className="text-center">Our Products</h1>
			
			<Row>
			{products}
			</Row>
			

		</>
	)
} 
